%global unicodedir %{_datadir}/unicode
%global emojidir %{unicodedir}/emoji

Name:           unicode-emoji
Version:        10.90.20180207
Release:        1%{?dist}
Summary:        Unicode Emoji Data Files

License:        Unicode
URL:            http://www.unicode.org/emoji/
Source0:        http://www.unicode.org/copyright.html
Source1:        http://www.unicode.org/Public/emoji/11.0/ReadMe.txt
Source2:        http://www.unicode.org/Public/emoji/11.0/emoji-data.txt
Source3:        http://www.unicode.org/Public/emoji/11.0/emoji-sequences.txt
Source4:        http://www.unicode.org/Public/emoji/11.0/emoji-test.txt
Source5:        http://www.unicode.org/Public/emoji/11.0/emoji-variation-sequences.txt
Source6:        http://www.unicode.org/Public/emoji/11.0/emoji-zwj-sequences.txt
BuildArch:      noarch

%description
Unicode Emoji Data Files are the machine-readable
emoji data files associated with
http://www.unicode.org/reports/tr51/index.html

%prep
%{nil}

%build
%{nil}

%install
cp -p %{SOURCE0} .
mkdir -p %{buildroot}%{emojidir}
cp -p %{SOURCE1} %{buildroot}%{emojidir}
cp -p %{SOURCE2} %{buildroot}%{emojidir}
cp -p %{SOURCE3} %{buildroot}%{emojidir}
cp -p %{SOURCE4} %{buildroot}%{emojidir}
cp -p %{SOURCE5} %{buildroot}%{emojidir}
cp -p %{SOURCE6} %{buildroot}%{emojidir}

%files
%license copyright.html
%dir %{unicodedir}
%dir %{emojidir}
%doc %{emojidir}/ReadMe.txt
%{emojidir}/emoji-*txt

%changelog
* Wed Mar 07 2018 Mike FABIAN <mfabian@redhat.com> - 10.90.20180207-1
- Update to a prerelease of Unicode Emoji Data 11.0

* Fri Feb 09 2018 Fedora Release Engineering <releng@fedoraproject.org> - 5.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Thu Jul 27 2017 Fedora Release Engineering <releng@fedoraproject.org> - 5.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Tue Jun 20 2017 Mike FABIAN <mfabian@redhat.com> - 5.0-1
- update to Unicode Emoji Data 5.0

* Thu May 04 2017 Mike FABIAN <mfabian@redhat.com> - 4.0-3
- add directory %%{emojidir} to file list

* Tue May 02 2017 Mike FABIAN <mfabian@redhat.com> - 4.0-2
- Fix rpmlint issues: description-line-too-long, corrected
  license tag, tag ReadMe.txt as %%doc

* Tue Apr 25 2017 Mike FABIAN <mfabian@redhat.com> - 4.0-1
- package Unicode Emoji Data 4.0
- MIT license
